<!-- <?php 
	$txt = "Hello World!";
	$number = 10;

	echo $txt;
	echo "<br>";
	echo $number;

	echo "<h4>This is a simple heading.</h4>";
	echo "<h4 style = 'color:red;'>This is heading with style<h4/>";

	$a = 123;
	var_dump($a);
	echo "<br>";

	$color = array("Red","Green","Blue");
	var_dump($color);
	echo "<br>";

	$color_code = array(
		"Red" => "#ff0000",
		"Green" => "#00ff00",
		"Blue" => "#0000ff"
	);
	var_dump($color_code);
	echo "<br>";

	class greeting{
		public $str = "Hello World";
		function show_greeting(){
			return $this -> str;
		}
	}

	$message = new greeting;
	var_dump($message);
	echo "<br>";
	echo $message -> show_greeting();
 ?> -->
<!DOCTYPE html>
<html>
<head>
	<title>Example of PHP POST method</title>
</head>
<body>
	<?php
		if(isset($_REQUEST["name"])){
			echo "<p>Hi,".$_REQUEST["name"]."</p>";
		}
	?>
	<form method="post" action="<?php echo $SERVER['PHP_SELF'];?>">
		<label for="inputName">Name</label>
		<input type="text" name="name" id="inputName">
		<input type="submit" name="Submit">
	</form>
</body>
</html>